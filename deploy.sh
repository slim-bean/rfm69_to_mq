#!/bin/bash -e

HOST=$1

cargo deb --target=armv7-unknown-linux-gnueabihf
scp target/armv7-unknown-linux-gnueabihf/debian/rfm69_to_mq_0.1.0_armhf.deb  pi@${HOST}:
ssh pi@${HOST} "sudo dpkg -i rfm69_to_mq_0.1.0_armhf.deb"