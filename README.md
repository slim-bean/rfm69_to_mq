

## Cross compiling


Download cross compiler

https://releases.linaro.org/components/toolchain/binaries/


Create `~/.cargo/config`

```
[target.armv7-unknown-linux-gnueabihf]
linker = "/home/user/cc/gcc-linaro-7.3.1-2018.05-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-gcc-7.3.1"
```

Add the target to rust

```
rustup override set nightly
rustup target add armv7-unknown-linux-gnueabihf
```

I had zero luck trying to cross compile the mosquitto library, there is some support for this, however it would also require cross compiling the necessary deps like openssl, which is possible, however I see no where in the Make config for mosquitto to tell it how to use your cross compiled deps.

In the end I said fuck it and just pulled the pre-compiled lib and deps from debian by adding the armhf arch to my build machine and doing apt download with a dpkg -x to get all the necessary so's

You then need to tell cargo how to tell rust how to find them, this is done in the `build.rs` script

```
sudo dpkg --add-architecture armhf
sudo apt update
apt download libmosquitto-dev:armhf
apt download libmosquitto1:armhf
apt download libc-ares2:armhf
apt download libc-ares-dev:armhf
apt download libssl1.1:armhf
apt download libssl-dev:armhf
dpkg -x *.deb .
```

Make sure the cross compiler is in the path, needed for some libs like log4rs

```shell
export PATH=$PATH:/home/user/cc/gcc-linaro-7.3.1-2018.05-x86_64_arm-linux-gnueabihf/bin/
```

You should now be able to build for the raspberry pi like so:

```
cargo build --target=armv7-unknown-linux-gnueabihf
```

or

```
cargo build --target=armv7-unknown-linux-gnueabihf --release
```

or to build the deb:

```
cargo deb --target=armv7-unknown-linux-gnueabihf
```


If you don't want to cross compile at all, you can probably just `apt install` all those deps and just run `cargo build` without specifying a target.

If you are trying to build this for another architecture, you will need to update build.rs with additional entries for your arch as well as provide the proper cross compiled shared objects for linking.

Or try your hand at one of the pure rust mqtt implementations, I was not impressed by any however, most don't seem to have any active support. :(

## Ras Pi Setup

```
sudo apt install libmosquitto-dev
```

## Tests

To run the tests you'll have to install libmosquitto-dev on your build machine since it's needed to compile.

```
sudo apt install libmosquitto-dev
```

You won't be able to use the cross-compiled targets for testing

## AES Encryption Key

To keep the AES key for the wireless communication out of source control, I marked a file called `key.txt` in the root of the project in `.gitignore`

In this file put the 32 character, 16 byte hex key.  All on one line, no spaces, no newline, just like this:

```nohighlight
A1A2A3A4A5A6A7A8B1B2B3B4B5B6B7B8
```