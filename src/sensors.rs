use serde_yaml;
use itoa;
use std::fmt::Error as FmtError;
use sensor_lib::{SensorValue, SensorFormat, SensorDefinition, load_from_file};
use std::time::SystemTime;
use std::collections::HashMap;


#[derive(Debug)]
pub enum SensorError {
    PayloadLengthError(Vec<SensorHolder>),
    IdNotFound(Vec<SensorHolder>),
    PayloadContentsError(Vec<SensorHolder>),
}



#[derive(Debug)]
pub struct SensorHolder {
    pub sensor_value: SensorValue,
    pub destination_queues: Vec<String>,
}


enum ParseError {
    NoMoreData,
    PayloadTooShort,
    IdNotFound,
    IntegerParseError
}

impl From<FmtError> for ParseError {
    fn from(_: FmtError) -> Self {
        ParseError::IntegerParseError
    }
}

pub struct Sensors {
    sensors: HashMap<i16, SensorDefinition>,
}

impl Sensors {
    pub fn parse_payload(&self, payload: &Vec<u8>) -> Result<Vec<SensorHolder>, SensorError> {

        let mut sensor_readings = Vec::new();
        //Clone the payload because we are going to modify this vec and we only have a borrowed ref to it
        let mut payload_copy = payload.clone();
        loop {
            //With each iteration we hand the ownership of the payload over to the parse function
            match self.parse(payload_copy){
                Ok((reading, remaining)) => {
                    sensor_readings.push(reading);
                    payload_copy = remaining;
                },
                Err(err) => match err {
                    ParseError::NoMoreData => {
                        return Ok(sensor_readings);
                    },
                    ParseError::PayloadTooShort => {
                        //Return anything we were able to read
                        return Err(SensorError::PayloadLengthError(sensor_readings));
                    },
                    ParseError::IdNotFound => {
                        //Return anything we were able to read
                        return Err(SensorError::IdNotFound(sensor_readings));
                    },
                    ParseError::IntegerParseError => {
                        return Err(SensorError::PayloadContentsError(sensor_readings));
                    }
                },
            }
        }
    }

    //TODO there is probably a way with lifetimes to send a reference to the queue names without having to clone them
    //However this also seems like an optimization I just don't need at this point and time.
    fn parse(&self, buff: Vec<u8>) -> Result<(SensorHolder, Vec<u8>), ParseError>{
        let mut itr = buff.iter();
        let id1 = itr.next().ok_or(ParseError::NoMoreData)?;
        let id2 = itr.next().ok_or(ParseError::PayloadTooShort)?;
        let id: i16 = ((id1.clone() as i16) << 8) + ((id2.clone() as i16) << 0);
        let sensor: &SensorDefinition = self.sensors.get(&id).ok_or(ParseError::IdNotFound)?;
        let sensor_holder = match sensor.format {
            SensorFormat::Bool => {
                let byte1 = *itr.next().ok_or(ParseError::PayloadTooShort)?;
                if byte1 == 1 {
                    SensorHolder {
                        sensor_value: SensorValue {
                            id,
                            timestamp: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() as u64,
                            value: String::from("1")
                        },
                        destination_queues: sensor.destination_queues.clone()
                    }
                } else {
                    SensorHolder {
                        sensor_value: SensorValue {
                            id,
                            timestamp: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() as u64,
                            value: String::from("0")
                        },
                        destination_queues: sensor.destination_queues.clone()
                    }
                }

            },
            SensorFormat::TwoByteFloat => {
                //The iterator only returns references to the objects in the array, we need to dereference to use the itoa function which needs the actual value
                let byte1 = *itr.next().ok_or(ParseError::PayloadTooShort)? as i8;
                let mut byte2 = *itr.next().ok_or(ParseError::PayloadTooShort)? as i8;
                let mut int_val = String::new();
                itoa::fmt(&mut int_val, byte1)?;
                let mut frac_val = String::new();
                //FIXME this is a fix for a bug in the remote station which is currently sending negative decimal values, this should someday be removed
                byte2 = byte2.abs();
                itoa::fmt(&mut frac_val, byte2)?;

                int_val.push('.');
                int_val.push_str(format!("{:0>2}", &frac_val).as_str());
                SensorHolder {
                    sensor_value: SensorValue {
                        id,
                        timestamp: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() as u64,
                        value: int_val
                    },
                    destination_queues: sensor.destination_queues.clone()
                }

            },
            SensorFormat::FourByteInt => {
                let byte1 = *itr.next().ok_or(ParseError::PayloadTooShort)?;
                let byte2 = *itr.next().ok_or(ParseError::PayloadTooShort)?;
                let byte3 = *itr.next().ok_or(ParseError::PayloadTooShort)?;
                let byte4 = *itr.next().ok_or(ParseError::PayloadTooShort)?;
                let val: u32 = ((byte1.clone() as u32) << 24) + ((byte2.clone() as u32) << 16) + ((byte3.clone() as u32) << 8) + ((byte4.clone() as u32) << 0);
                let mut string_val = String::new();
                itoa::fmt(&mut string_val, val);
                SensorHolder {
                    sensor_value: SensorValue {
                        id,
                        timestamp: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() as u64,
                        value: string_val
                    },
                    destination_queues: sensor.destination_queues.clone()
                }
            },
            SensorFormat::TwoByteInt => {
                let byte1 = *itr.next().ok_or(ParseError::PayloadTooShort)?;
                let byte2 = *itr.next().ok_or(ParseError::PayloadTooShort)?;
                let val: u16 = ((byte1.clone() as u16) << 8) + ((byte2.clone() as u16) << 0);
                let mut string_val = String::new();
                itoa::fmt(&mut string_val, val);
                SensorHolder {
                    sensor_value: SensorValue {
                        id,
                        timestamp: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() as u64,
                        value: string_val
                    },
                    destination_queues: sensor.destination_queues.clone()
                }
            },
            SensorFormat::SingleByteInt => {
                let byte1 = *itr.next().ok_or(ParseError::PayloadTooShort)?;
                let mut string_val = String::new();
                itoa::fmt(&mut string_val, byte1);
                SensorHolder {
                    sensor_value: SensorValue {
                        id,
                        timestamp: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() as u64,
                        value: string_val
                    },
                    destination_queues: sensor.destination_queues.clone()
                }
            }
        };

        //If we call collect directly on the iterator, it only returns references, where we actually
        //want a new array of the remaining values, so we have to clone what's left and then collect
        let remaining = itr.cloned().collect::<Vec<u8>>();
        Ok((sensor_holder, remaining))
    }

    pub fn new(yaml_file: &str) -> Sensors {

        let sensors_map = load_from_file(yaml_file);

        let sensors = Sensors {
            sensors: sensors_map,
        };
        sensors
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;
    use sensor_lib::MetricType;

    #[test]
    fn parse_success() {
        let payload = vec![0u8, 0, 4, 1, 0, 1, 25, 37];

        let sensor1 = SensorDefinition{
            id: 0,
            name: String::new(),
            format: SensorFormat::TwoByteFloat,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let sensor2 = SensorDefinition{
            id: 1,
            name: String::new(),
            format: SensorFormat::TwoByteFloat,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let mut sensors_map = HashMap::new();
        sensors_map.insert(0, sensor1);
        sensors_map.insert(1, sensor2);

        let sensors = Sensors{
            sensors: sensors_map,
        };

        let packets = sensors.parse_payload(&payload).unwrap();

        assert_eq!(2, packets.len());
        assert_eq!("4.01", packets[0].sensor_value.value);
        assert_eq!("25.37", packets[1].sensor_value.value);

    }

    #[test]
    fn parse_payload_too_short() {
        let payload = vec![0u8, 0, 4, 1, 0, 1, 25];

        let sensor1 = SensorDefinition{
            id: 0,
            name: String::new(),
            format: SensorFormat::TwoByteFloat,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let sensor2 = SensorDefinition{
            id: 1,
            name: String::new(),
            format: SensorFormat::TwoByteFloat,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let mut sensors_map = HashMap::new();
        sensors_map.insert(0, sensor1);
        sensors_map.insert(1, sensor2);


        let sensors = Sensors{
            sensors: sensors_map,
        };

        match sensors.parse_payload(&payload) {
            Ok(_) => {
                panic!("Should have returned error");
            },
            Err(error) => match error {
                SensorError::PayloadLengthError(parsed_payload) => {
                    assert_eq!(1, parsed_payload.len());
                    assert_eq!("4.01", parsed_payload[0].sensor_value.value);
                },
                SensorError::IdNotFound(_) => {
                    panic!("Not the error we were expecting");
                },
                SensorError::PayloadContentsError(_) => {
                    panic!("Not the error we were expecting");
                },
            },
        };
    }

    #[test]
    fn parse_two_byte_float(){
        let payload = vec![0u8, 0, 23, 99, 0, 1, 1, 1];

        let sensor1 = SensorDefinition{
            id: 0,
            name: String::new(),
            format: SensorFormat::TwoByteFloat,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let sensor2 = SensorDefinition{
            id: 1,
            name: String::new(),
            format: SensorFormat::TwoByteFloat,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let mut sensors_map = HashMap::new();
        sensors_map.insert(0, sensor1);
        sensors_map.insert(1, sensor2);


        let sensors = Sensors{
            sensors: sensors_map,
        };

        let packets = sensors.parse_payload(&payload).unwrap();

        assert_eq!(2, packets.len());
        assert_eq!("23.99", packets[0].sensor_value.value);
        assert_eq!("1.01", packets[1].sensor_value.value);
    }

    #[test]
    fn parse_neg_two_byte_float(){
        let payload = vec![0u8, 0, 0b11101001, 99, 0, 1, 1, 1];

        let sensor1 = SensorDefinition{
            id: 0,
            name: String::new(),
            format: SensorFormat::TwoByteFloat,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let sensor2 = SensorDefinition{
            id: 1,
            name: String::new(),
            format: SensorFormat::TwoByteFloat,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let mut sensors_map = HashMap::new();
        sensors_map.insert(0, sensor1);
        sensors_map.insert(1, sensor2);


        let sensors = Sensors{
            sensors: sensors_map,
        };

        let packets = sensors.parse_payload(&payload).unwrap();

        assert_eq!(2, packets.len());
        assert_eq!("-23.99", packets[0].sensor_value.value);
        assert_eq!("1.01", packets[1].sensor_value.value);
    }

    #[test]
    fn parse_neg_two_byte_float_2(){

        let temp = -1.72f32;
        let msb: u8 = temp.clone() as u8;
        let lsb: u8 = (temp as u8 - msb) * 100;
        //radiopacket[11] = (byte)((htu_temp - radiopacket[10]) * 100);

        //This is currently what's being sent because the remote station has a bug and is sending a negative decimal
        let payload = vec![0u8, 0, 255, 184, 0, 1, 1, 1];

        let sensor1 = SensorDefinition{
            id: 0,
            name: String::new(),
            format: SensorFormat::TwoByteFloat,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let sensor2 = SensorDefinition{
            id: 1,
            name: String::new(),
            format: SensorFormat::TwoByteFloat,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let mut sensors_map = HashMap::new();
        sensors_map.insert(0, sensor1);
        sensors_map.insert(1, sensor2);


        let sensors = Sensors{
            sensors: sensors_map,
        };

        let packets = sensors.parse_payload(&payload).unwrap();

        assert_eq!(2, packets.len());
        assert_eq!("-1.72", packets[0].sensor_value.value);
        assert_eq!("1.01", packets[1].sensor_value.value);
    }

    #[test]
    fn parse_max_two_byte_float(){
        let payload = vec![0u8, 0, 127, 99];

        let sensor1 = SensorDefinition{
            id: 0,
            name: String::new(),
            format: SensorFormat::TwoByteFloat,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let mut sensors_map = HashMap::new();
        sensors_map.insert(0, sensor1);


        let sensors = Sensors{
            sensors: sensors_map,
        };

        let packets = sensors.parse_payload(&payload).unwrap();

        assert_eq!(1, packets.len());
        assert_eq!("127.99", packets[0].sensor_value.value);
    }

    #[test]
    fn parse_min_two_byte_float(){
        let payload = vec![0u8, 0, 0b10000000, 99];

        let sensor1 = SensorDefinition{
            id: 0,
            name: String::new(),
            format: SensorFormat::TwoByteFloat,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let mut sensors_map = HashMap::new();
        sensors_map.insert(0, sensor1);


        let sensors = Sensors{
            sensors: sensors_map,
        };

        let packets = sensors.parse_payload(&payload).unwrap();

        assert_eq!(1, packets.len());
        assert_eq!("-128.99", packets[0].sensor_value.value);
    }

    #[test]
    fn parse_single_byte_int(){
        let payload = vec![0u8, 0, 255, 0, 1, 0];

        let sensor1 = SensorDefinition{
            id: 0,
            name: String::new(),
            format: SensorFormat::SingleByteInt,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let sensor2 = SensorDefinition{
            id: 1,
            name: String::new(),
            format: SensorFormat::SingleByteInt,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let mut sensors_map = HashMap::new();
        sensors_map.insert(0, sensor1);
        sensors_map.insert(1, sensor2);

        let sensors = Sensors{
            sensors: sensors_map,
        };

        let packets = sensors.parse_payload(&payload).unwrap();

        assert_eq!(2, packets.len());
        assert_eq!("255", packets[0].sensor_value.value);
        assert_eq!("0", packets[1].sensor_value.value);
    }

    #[test]
    fn parse_two_byte_int(){
        let payload = vec![0u8, 0, 0, 255, 0, 1, 178, 114];

        let sensor1 = SensorDefinition{
            id: 0,
            name: String::new(),
            format: SensorFormat::TwoByteInt,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let sensor2 = SensorDefinition{
            id: 1,
            name: String::new(),
            format: SensorFormat::TwoByteInt,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let mut sensors_map = HashMap::new();
        sensors_map.insert(0, sensor1);
        sensors_map.insert(1, sensor2);

        let sensors = Sensors{
            sensors: sensors_map,
        };

        let packets = sensors.parse_payload(&payload).unwrap();

        assert_eq!(2, packets.len());
        assert_eq!("255", packets[0].sensor_value.value);
        assert_eq!("45682", packets[1].sensor_value.value);
    }

    #[test]
    fn parse_four_byte_int(){
        let payload = vec![0u8, 0, 255, 255, 255, 255, 0, 1, 0, 65, 137, 54];

        let sensor1 = SensorDefinition{
            id: 0,
            name: String::new(),
            format: SensorFormat::FourByteInt,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let sensor2 = SensorDefinition{
            id: 1,
            name: String::new(),
            format: SensorFormat::FourByteInt,
            destination_queues: Vec::new(),
            metric_name: String::new(),
            metric_type: MetricType::Gauge,
            metric_labels: HashMap::new(),
        };

        let mut sensors_map = HashMap::new();
        sensors_map.insert(0, sensor1);
        sensors_map.insert(1, sensor2);

        let sensors = Sensors{
            sensors: sensors_map,
        };

        let packets = sensors.parse_payload(&payload).unwrap();

        assert_eq!(2, packets.len());
        assert_eq!("4294967295", packets[0].sensor_value.value);
        assert_eq!("4294966", packets[1].sensor_value.value);
    }
}