use std::collections::HashMap;

pub struct ExpiringMap {
    map: HashMap<u8, Vec<u32>>,

}

impl ExpiringMap {
    pub fn new() -> ExpiringMap {
        let map = ExpiringMap {
            map: HashMap::new()
        };
        map
    }

    pub fn seen(&mut self, from: u8, id: u32) -> bool {
        let id_vec = self.map.entry(from).or_insert(vec![]);
        if id_vec.contains(&id) {
            return true;
        }

        id_vec.push(id);
        if id_vec.len() > 10 {
            id_vec.remove(0);
        }

        false
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_expiring_map() {
        let mut map = ExpiringMap::new();

        assert_eq!(map.seen(1, 1), false);
        assert_eq!(map.seen(1, 1), true);
        assert_eq!(map.seen(1, 2), false);
        assert_eq!(map.seen(2, 1), false);
        assert_eq!(map.seen(1, 2), true);
        assert_eq!(map.seen(2, 1), true);
        assert_eq!(map.seen(1, 1), true);
    }

    #[test]
    fn test_map_keeps_vals() {
        let mut map = ExpiringMap::new();

        map.seen(100, 20);
        map.seen(100, 21);
        map.seen(100, 22);
        map.seen(100, 23);
        map.seen(100, 24);
        map.seen(100, 25);
        map.seen(100, 26);
        map.seen(100, 27);
        map.seen(100, 28);
        map.seen(100, 29);

        //Only keep 10 values so at this point we should still see our first entry
        assert_eq!(map.seen(100, 20), true);

        //Add an 11th value which should cause our first value to be removed
        map.seen(100, 30);

        //Check that the first value should now return false
        assert_eq!(map.seen(100, 20), false);
    }
}