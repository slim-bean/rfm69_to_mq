#![feature(duration_as_u128)]

extern crate sensor_lib;

extern crate embedded_hal as hal;
extern crate log4rs;
use log4rs::init_file;
extern crate linux_embedded_hal as linux_hal;
extern crate rppal;
#[macro_use]
extern crate log;
extern crate rfm69;
extern crate mosquitto_client;
#[macro_use]
extern crate serde_derive;
extern crate serde_yaml;
extern crate serde_json;
extern crate itoa;
extern crate hex;

mod sensors;
mod expiring_map;

use std::any::Any;
use linux_hal::{Pin, Spidev};
use linux_hal::spidev::{self, SpidevOptions};
use linux_hal::sysfs_gpio::{Direction, Edge};
use linux_hal::sysfs_gpio::Pin as Sysfs_Pin;
use hal::blocking::spi;
use hal::digital::OutputPin;
use rfm69::{HighPower, RFM69, Rfm69Error, Timer};
use std::thread;
use std::time::{Duration, Instant};
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::fmt::Debug;
use sensors::{Sensors, SensorHolder};
use expiring_map::ExpiringMap;
use sensor_lib::{LightValue, TempHumidityValue, WindSpeedDirValue};
use mosquitto_client::Mosquitto;
use rppal::gpio::{Gpio,Trigger,Level};

const INTERRUPT_PIN: u8 = 22;

struct PiTimer;

impl Timer for PiTimer {
    type Instant = Instant;

    fn now(&self) -> Instant {
        Instant::now()
    }

    fn since(&self, past: &Instant) -> Duration {
        past.elapsed()
    }
}


fn main() {

    //Check if there is a logging config configured in /etc if not just use one local to the app
    let etc_config = Path::new("/etc/rfm69_to_mq/log4rs.yml");
    let log_config_path = if let true = etc_config.exists() {
        etc_config
    } else {
        Path::new("log4rs.yml")
    };
    log4rs::init_file(log_config_path, Default::default()).expect("Failed to init logger");

    //TODO this will need to be loaded from a file
    let address: u8 = 1;

    info!("Application Starting with wireless address {}", address);

    info!("Loading AES Key");
    //Load up the AES Key
    let mut f = File::open("/etc/rfm69_to_mq/key.txt").expect("Did not find encryption key file: /etc/rfm69_to_mq/key.txt");
    let mut key_text = String::new();
    let result = f.read_to_string(&mut key_text).expect("Failed to read /etc/rfm69_to_mq/key.txt");
    let aes_key_vec = hex::decode(key_text).expect("Failed to parse key to byte array, make sure the key is exactly 16 bytes with no spaces or 0x or anything, just 32 chars");
    if aes_key_vec.len() < 16 {
        panic!("The AES key must be 16 bytes long")
    }
    let mut aes_key = [0u8; 16];
    for i in 0..16 {
        aes_key[i] = *aes_key_vec.get(i).unwrap();
    }

    //debug!("AES Key {:?}", aes_key);


    info!("Loading sensors file");
    let sensors = Sensors::new("/etc/rfm69_to_mq/sensors.yml");

    //Setup the spi linux_embedded_hal implementation of embedded_hal
    info!("Configuring SPI");
    let mut spi = Spidev::open("/dev/spidev0.0").expect("SPI open error");
    let options = SpidevOptions::new()
        .max_speed_hz(1_000_000)
        .mode(spidev::SPI_MODE_0)
        .build();
    spi.configure(&options).unwrap();

    info!("Configuring chipselect pin");
    let cs_receive = Pin::new(25);
    cs_receive.export().unwrap();
    while !cs_receive.is_exported() {};
    cs_receive.set_direction(Direction::Out).unwrap();
    cs_receive.set_value(1).unwrap();

    info!("Configuring reset pin");
    let reset = Pin::new(24);
    reset.export().unwrap();
    while !reset.is_exported() {};
    reset.set_direction(Direction::Out).unwrap();
    reset.set_value(0).unwrap();

    info!("Resetting the radio");
    thread::sleep(Duration::from_millis(5));
    reset.set_value(1).unwrap();
    thread::sleep(Duration::from_millis(5));
    reset.set_value(0).unwrap();
    thread::sleep(Duration::from_millis(5));

    info!("Constructing/Configuring the RFM69 Driver");
    let mut rfm69_receive = RFM69::<_, _, _, HighPower>::new(spi, cs_receive, PiTimer).unwrap();
    rfm69_receive.aes_on(&aes_key).unwrap();

    info!("Configuring the receive interrupt");
    let gpio = Gpio::new().expect("Failed to get GPIO device");
    let mut interrupt = gpio.get(INTERRUPT_PIN).expect("Failed to get GPIO PIN").into_input_pulldown();
    interrupt.set_interrupt(Trigger::RisingEdge).expect("Failed to set interrupt on PIN");

    let m = mosquitto_client::Mosquitto::new("rfm69_to_mq");

    m.connect("localhost", 1883).unwrap();

    let mut message_map = ExpiringMap::new();

    info!("Starting the main loop;");
    let interrupt_pins = [&interrupt];
    //Main loop
    loop {
        //debug!("Setting radio into receive mode and waiting on interrupt");
        rfm69_receive.receive_async_start().unwrap();
        //Wait for an interrupt, breaking every second or so for a heartbeat
        match gpio.poll_interrupts(&interrupt_pins,true, Some(Duration::from_millis(1000))) {
            Ok(opt) => {
                match opt {
                    Some(pin) => {
                        match pin.0.pin() {
                            INTERRUPT_PIN => {
                                //An interrupt occurred, we need to check the value
                                receive_packet(&mut rfm69_receive, address, &m, &mut message_map, &sensors);
                            }
                            _ => {}
                        }
                    }
                    None => {
                        //Timeout, no interrupt
                        debug!("main loop interrupt poll time expired with no interrupt");
                        if interrupt.is_high() {
                            warn!("missed an interrupt! reading data");
                            receive_packet(&mut rfm69_receive, address, &m, &mut message_map, &sensors);
                        }
                    }
                }
            },
            Err(_) => {},
        }
        //TODO heartbeat
    };
}

#[derive(Debug)]
struct Packet {
    to: u8,
    from: u8,
    id: u8,
    flags: u8,
    unique_id: u32,
    payload: Vec<u8>,
}



fn receive_packet<SPI, NCS, T, PA, E>(rfm69_receive: &mut RFM69<SPI, NCS, T, PA>, address: u8, m: &Mosquitto, message_map: &mut ExpiringMap, sensors: &Sensors  )
where
    SPI: spi::Write<u8, Error = E> + spi::Transfer<u8, Error = E>,
    NCS: OutputPin,
    T: Timer,
    PA: Any,
    E: Debug,
{
    //66 bytes is the max number of bytes that can be in the FIFO with AES enabled
    let mut buf = [0 as u8; 66];

    match rfm69_receive.receive_async_read(&mut buf) {
        Ok(length) => {
            debug!("Received {}; byte packet", length);
            match process_packet(address, buf, length) {
                Some(packet) => {
                    info!("Received From '{}' a {} byte packet with unique_id '{}' and RSSI {}", packet.from, length, packet.unique_id, rfm69_receive.rssi());

                    if !message_map.seen(packet.from, packet.unique_id) {

                        //Convert the packet and send it to our topic
                        debug!("Payload {:?}", packet.payload);
                        let now = Instant::now();
                        let readings = sensors.parse_payload(&packet.payload).unwrap();
                        debug!("Parsed in {}us", now.elapsed().as_micros());

                        debug!("Readings {:?}", readings);
                        let mut ws1_light_val = LightValue {
                            timestamp: 0,
                            location: 0,
                            uv_raw: 0,
                            uv_index: 0.0,
                            vis_raw: 0,
                            ir_raw: 0,
                            lux: 0,
                        };

                        let mut ws1_temp_hum_val = TempHumidityValue {
                            timestamp: 0,
                            location: 0,
                            temp: 0.0,
                            humidity: 0.0,
                        };

                        let mut ws4_wind_spd_dir_val = WindSpeedDirValue {
                            timestamp: 0,
                            location: 0,
                            speed: 0,
                            dir: 0,
                        };

                        for reading in readings {
                            populate_light_value(&mut ws1_light_val, &reading);
                            populate_temp_humidity_value(&mut ws1_temp_hum_val, &reading);
                            populate_wind_speed_dir_value(&mut ws4_wind_spd_dir_val, &reading);

                            for queue in reading.destination_queues {
                                match serde_json::to_string(&reading.sensor_value) {
                                    Ok(val) => {
                                        send_to_topic(m, &queue, val.as_bytes());
                                    }
                                    Err(err) => {
                                        error!("Failed to serialize the sensor value: {}", err);
                                    }
                                }
                            }
                        }

                        //FIXME this all seems kind of gross how rigidly coupled this is....
                        //Send the LightValue packet
                        if ws1_light_val.timestamp != 0 {
                            match serde_json::to_string(&ws1_light_val) {
                                Ok(val) => {
                                    send_to_topic(m, "/ws/1/grp/light", val.as_bytes());
                                }
                                Err(err) => {
                                    error!("Failed to serialize the LightValue: {}", err);
                                }
                            }
                        }

                        //Send the TempHumidity packet
                        if ws1_temp_hum_val.timestamp != 0 {
                            match serde_json::to_string(&ws1_temp_hum_val) {
                                Ok(val) => {
                                    send_to_topic(m, "/ws/1/grp/temp_humidity", val.as_bytes());
                                }
                                Err(err) => {
                                    error!("Failed to serialize the LightValue: {}", err);
                                }
                            }
                        }

                        if ws4_wind_spd_dir_val.timestamp != 0 {
                            match serde_json::to_string(&ws4_wind_spd_dir_val) {
                                Ok(val) => {
                                    send_to_topic(m, "/ws/4/grp/wind_speed_dir", val.as_bytes());
                                }
                                Err(err) => {
                                    error!("Failed to serialize the WindSpeedDirValue: {}", err);
                                }
                            }
                        }

                    } else {
                        info!("Received duplicate message from {} with id {}, re-sending ack", packet.from, packet.unique_id)
                    }


                    //The first bit in the flags is defined by us to indicate if the sender is waiting for an ack
                    //If this bit is set, they are not, so here we check the bit is not set, and send an ack
                    if !((packet.flags & 0b1) == 0b1){
                        debug!("Sending ACK back to {}", packet.from);
                        let buf = [5, packet.from, address, packet.id, 0x80, 33];
                        rfm69_receive.send(&buf).unwrap();
                        debug!("ACK Sent");
                    }
                    debug!("Finished processing packet");
                }
                None => {
                    debug!("Packet was either not for us or malformed");
                }
            }
        }
        Err(error) => match error {
            Rfm69Error::PacketNotReady => {
                //This happens after almost every packet, not sure if the IRQ is noisy and not debounced
                //or maybe there is a race condition on when it gets cleared vs when we re-enable our receiver
                //either way, we basically just ignore this and return to listening for packets
                debug!("When we went to read the packet, the packet ready irq flag was clear");
            }
            Rfm69Error::UnderlyingLibError(cause) => {
                panic!("There was an underlying IO problem trying to read the message: {:?}", cause);
            }
            _ => {
                panic!("Another unexpected error occured: {:?}", error);
            }
        }
    };
}

fn process_packet(address: u8, buf: [u8; 66], length: u8) -> Option<Packet> {
    if length < 8 {
        //Each packet would need to be this format:
        // | to | from | id | flags | unique_id 1 | unique_id 2 | unique_id 3 | unique_id 4 |
        //Which is a minimum of 8 bytes
        warn!("Received a packet less than 4 bytes long, ignoring: {}, {}, {}, {}, {}, {}, {}, {}", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5], buf[6], buf[7], );
        return None;
    }

    if length > 66 {
        //Also ignore anything longer than 66 bytes because though I'm not sure this is actually possible anyway
        warn!("Received a packet more than 66 bytes long, ignoring");
        return None;
    }


    //Extract the header values
    let to = buf[0];
    let from = buf[1];
    let id = buf[2];
    let flags = buf[3];

    //Re-assemble the unique_id
    let unique_id: u32 = ((buf[4] as u32) << 24) + ((buf[5] as u32) << 16) + ((buf[6] as u32) << 8) + ((buf[7] as u32) << 0);


    debug!("Received packet from {} with id {}, length {}, and unique_id {}", from, id, length, unique_id);

    //Never ACK an ACK
    if !((flags & 0b10000000) == 0b10000000) {
        // Its a normal message not an ACK
        if to == address {
            // Its for this node and
            // Its not a broadcast, so ACK it
            // Acknowledge message with ACK set in flags and ID set to received ID
            let mut payload = Vec::new();
            for i in 8..length {
                payload.push(buf[i as usize]);
            }
            return Some(Packet {
                to,
                from,
                id,
                flags,
                unique_id,
                payload,
            });
        }
    }
    // No message for us available
    None
}

fn populate_light_value(light_value: &mut LightValue, reading: &SensorHolder) {
    if reading.sensor_value.id == 4 {
        light_value.location = 1;
        light_value.timestamp = reading.sensor_value.timestamp;
        match reading.sensor_value.value.parse() {
            Ok(val) => { light_value.uv_raw = val }
            Err(err) => { error!("Failed to convert uv_raw reading to int: {}", err) }
        }
    } else if reading.sensor_value.id == 5 {
        match reading.sensor_value.value.parse() {
            Ok(val) => { light_value.uv_index = val }
            Err(err) => { error!("Failed to convert uv_index reading to float: {}", err) }
        }
    } else if reading.sensor_value.id == 7 {
        match reading.sensor_value.value.parse() {
            Ok(val) => { light_value.vis_raw = val }
            Err(err) => { error!("Failed to convert vis_raw reading to int: {}", err) }
        }
    } else if reading.sensor_value.id == 8 {
        match reading.sensor_value.value.parse() {
            Ok(val) => { light_value.ir_raw = val }
            Err(err) => { error!("Failed to convert ir_raw reading to int: {}", err) }
        }
    } else if reading.sensor_value.id == 9 {
        match reading.sensor_value.value.parse() {
            Ok(val) => { light_value.lux = val }
            Err(err) => { error!("Failed to convert lux_raw reading to int: {}", err) }
        }
    }
}

fn populate_temp_humidity_value(temp_hum_value: &mut TempHumidityValue, reading: &SensorHolder) {
    if reading.sensor_value.id == 2 {
        temp_hum_value.location = 1;
        temp_hum_value.timestamp = reading.sensor_value.timestamp;
        match reading.sensor_value.value.parse() {
            Ok(val) => { temp_hum_value.temp = val }
            Err(err) => { error!("Failed to convert temp reading to float: {}", err) }
        }
    } else if reading.sensor_value.id == 3 {
        match reading.sensor_value.value.parse() {
            Ok(val) => { temp_hum_value.humidity = val }
            Err(err) => { error!("Failed to convert humidity reading to float: {}", err) }
        }
    }
}

fn populate_wind_speed_dir_value(wind_speed_dir_val: &mut WindSpeedDirValue, reading: &SensorHolder) {
    if reading.sensor_value.id == 201 {
        wind_speed_dir_val.location = 4;
        wind_speed_dir_val.timestamp = reading.sensor_value.timestamp;
        match reading.sensor_value.value.parse() {
            Ok(val) => { wind_speed_dir_val.speed = val }
            Err(err) => { error!("Failed to convert wind speed reading to int: {}", err) }
        }
    } else if reading.sensor_value.id == 202 {
        match reading.sensor_value.value.parse() {
            Ok(val) => { wind_speed_dir_val.dir = val }
            Err(err) => { error!("Failed to convert wind direction reading to int: {}", err) }
        }
    }
}

fn send_to_topic(m: &Mosquitto, topic: &str, payload: &[u8]) {
    for i in 1..6 {
        match m.publish_wait(topic, payload, 2, false, 1000) {
            Ok(id) => {
                debug!("Message {} published successfully to {} after {} attempts", id, topic, i);
                if i > 1 {
                    info!("Message {} published successfully to {} after {} attempts", id, topic, i);
                }
                break;
            }
            Err(e) => {
                debug!("Failed to enqueue data: {} to topic {}, will retry {} more times", e, topic, 5 - i);
                if i == 5 {
                    error!("Failed to enqueue message after 5 tries to topic {}, it will be dropped", topic);
                    panic!("Failed to enqueue message after 5 tries to topic {}, it will be dropped", topic);
                }
            }
        };
    }
}

